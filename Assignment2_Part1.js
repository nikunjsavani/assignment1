function changePara1(){
    document.getElementById("para1").innerHTML="New Text!";
}
function insertCurrentDate(){
    document.getElementById("datePara").innerHTML=Date();
} 

//Restoring the change on button click even
function restorePara1(){
    document.getElementById("para1").innerHTML="Original Text";
}

function removeCurrentDate(){
    document.getElementById("datePara").innerHTML="";
}

//Change the style of paragraph
function changeParagraphStyle(Paragraph){
    Paragraph.style.fontWeight = "BOLD";
    Paragraph.style.fontFamily = "Lucida Console";
    var Context = Paragraph.innerHTML;
    Paragraph.innerHTML = "\""+ Context +"\"";
}

function resetParagraghStyle(Paragraph){
    Paragraph.style.fontWeight = "NORMAL";
    Paragraph.style.fontFamily = "Times New Roman";
    var Context = Paragraph.innerHTML;
    Context = Context.substring(1,Context.length-1);
    Paragraph.innerHTML = Context;
}
