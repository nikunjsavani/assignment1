//TO DO D: Configure the paths of the five images 
var slideimages =["images/Image1.jpg", 
                  "images/Image2.jpg", 
                  "images/Image3.jpg", 
                  "images/Image4.jpg", 
                  "images/Image5.jpg", 
                  "images/Image6.jpg", 
                  "images/Image7.jpg"];

//TO DO D: Configure URL to the five target links 
var slidelinks = ["https://unsplash.com/photos/uUi5RQCyUdM",
                  "https://unsplash.com/photos/OTgguozekvg",
                  "https://unsplash.com/photos/uzNCt8wmnPw",
                  "https://unsplash.com/photos/YApS6TjKJ9c",
                  "https://unsplash.com/photos/cyuRhbqG5AI",
                  "https://unsplash.com/photos/10tOJa4APL8",
                  "https://unsplash.com/photos/FMbWFDiVRPs"];

function gotoshow()
{
    if (!window.winslide || winslide.closed){
        winslide=window.open(slidelinks[index]);   
    }
    else{
        winslide.location=slidelinks[index];
        winslide.focus();
    }
}

//TO DO E: configure the speed of the slideshow, in milliseconds 
var slideshowspeed=2000;
var index=0;

function slideit()
{
    //TO DO F: update index so that the image keep rotating for ever
    //TO DO G: set the next image in the img 
    
    if(index==slideimages.length-1){
        index=0;
    }else{
        index++;
    }
    
    document.getElementById("slider").src = slideimages[index];
    //console.log(Date());
    setTimeout("slideit()",slideshowspeed);
}

//This is click event of btnChangeSpeed button
window.onload = (event) => {
    //This even will only occure once page is fully loaded
    document.getElementById("btnChangeSpeed").addEventListener("click", ChangeSpeed);
    document.getElementById("txtSpeed").value = slideshowspeed/1000;
}

//This function will check the input from user and if it is valid input, then it will change the speed
function ChangeSpeed(){
    var NewSpeed = document.getElementById("txtSpeed").value;
    //console.log('Old Speed :- ' + slideshowspeed);
    if(isNaN(NewSpeed)){
        alert("Please Enter Numberic Value!");
        document.getElementById("txtSpeed").value = slideshowspeed/1000;
    }else{
        if(NewSpeed >= 1 && NewSpeed <= 10){
            slideshowspeed = NewSpeed * 1000;
        }else{
            alert("Please Enter Number Between 1 - 10!");
            document.getElementById("txtSpeed").value = slideshowspeed/1000;
        }
    }
    //console.log('New Speed :- ' + slideshowspeed);
}